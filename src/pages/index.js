/* eslint-disable react/jsx-one-expression-per-line */
import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Head from "../components/head"

export const query = graphql`
  query {
    katastatiko: allFile(
      filter: { relativePath: { eq: "data/katastatiko.pdf" } }
    ) {
      edges {
        node {
          publicURL
        }
      }
    },
  }
`

const IndexPage = props => {
  const katastatiko = props.data.katastatiko.edges

  return (
    <Layout>
      <Head title="Home" />
      <div className="centered">
        <p>
          Η Αλέξανδρος Βασιλείου Μονοπρόσωπη Ι.Κ.Ε. δραστηριοποιείται στην
          παροχή υπηρεσιών εφαρμογών πληροφορικής.
        </p>
        <ul className="vim-categories">
          Βασικές πληροφορίες
          <li>
            Όνομα εταιρίας: <span>Αλέξανδρος Βασιλείου Μονοπρόσωπη Ι.Κ.Ε.</span>
          </li>
          <li>
            Διαχειριστής και μοναδικός εταίρος:
            <span> Αλέξανδρος Βασιλείου</span>
          </li>
          <ul>
            Στοιχεία επικοινωνίας:
            <li>
              Διεύθυνση: <span>Μεγάλου Αλεξάνδρου 35, 41222 Λάρισα</span>
            </li>
            <li>
              Τηλέφωνο: <span>2411810849</span>
            </li>
            <li>
              email: <span>alexandrosvasileiou@gmail.com</span>
            </li>
          </ul>
        </ul>
        <ul className="vim-categories">
          Ιδιοκτησία και Οικονομικά Στοιχεία
          <li>
            ΑΦΜ:
            <span> 801648013</span>
          </li>
          <li>
            ΔΟΥ:
            <span> Λάρισας</span>
          </li>
          <li>
            Εταιρικό Κεφάλαιο:
            <span> 3000€</span>
          </li>
          <li>
            Νομική Μορφή:
            <span> Μονοπρόσωπη Ι.Κ.Ε.</span>
          </li>
          <li>
            Διάρκεια Εταιρίας:
            <span> 12ετής</span>
          </li>
          <li>
            Αριθμός ΓΕΜΗ:
            <span> 160789940000</span>
          </li>
          <li>
            Ημερομηνία Σύστασης:
            <span> 21/09/2021</span>
          </li>
          <ul>
            Καταστατικό - Τροποποιήσεις:
            <li>
              {katastatiko.map(({ node }) => (
                <a href={node.publicURL} target="_blank" key={node.id}>
                  Κατεβάστε το αρχείο
                </a>
              ))}
            </li>
          </ul>
        </ul>

        <ul className="vim-categories">
          Περιγραφή των εργασιών της εταιρίας
          <li>Η παροχή υπηρεσιών εφαρμογών πληροφορικής</li>
          <li>Η παροχή υπηρεσιών γραφίστα ή μακετίστα, εκτός διαφήμισης</li>
          <li>Η κατασκευή παιχνιδιών κάθε είδους (Π.Δ.Κ.Α.)</li>
          <li>
            Λιανικό εμπόριο παιχνιδιών κάθε είδους, με αλληλογραφία ή μέσω
            διαδικτύου
          </li>
          <li>Η έκδοση λογισμικού εφαρμογών που κατεβάζεται από το internet</li>
          <li>Η παραγωγή πρωτότυπων λογισμικού</li>
        </ul>
      </div>
    </Layout>
  )
}

export default IndexPage
