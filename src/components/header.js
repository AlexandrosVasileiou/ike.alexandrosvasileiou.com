import React from "react"
import { ThemeToggler } from "gatsby-plugin-dark-mode"
import { Link } from "gatsby"

const Header = () => {
  return (
    <header className="header">
      <nav className="vim-navbar">
        <ul>
          <li>
            <Link className="text-[14px] md:text-[20px]" to="/">
              Αλέξανδρος Βασιλείου Μονοπρόσωπη Ι.Κ.Ε.
            </Link>
          </li>
          <li>
            <Link className="text-[14px] md:text-[20px]" to="/financials">
              Χρηματοοικονομικές Καταστάσεις
            </Link>
          </li>
        </ul>
      </nav>
      <nav className="color-mode hidden sm:block">
        <ul>
          <li>
            <ThemeToggler>
              {({ theme, toggleTheme }) => (
                <button
                  type="button"
                  onClick={() => {
                    toggleTheme(theme === "light" ? "dark" : "light")
                  }}
                >
                  {theme === "light" ? "dark theme" : "light theme"}
                </button>
              )}
            </ThemeToggler>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header
