// import React from "react"
// import { Link, graphql, useStaticQuery } from "gatsby"

// import Layout from "../components/layout"
// import { posts, post } from "./blog.module.scss"
// import Head from "../components/head"

// const BlogPage = () => {
//   const data = useStaticQuery(graphql`
//     query {
//       allMarkdownRemark(
//         sort: { fields: frontmatter___publishedDate, order: DESC }
//       ) {
//         edges {
//           node {
//             frontmatter {
//               title
//               publishedDate(formatString: "MMMM Do, YYYY")
//             }
//             fields {
//               slug
//             }
//           }
//         }
//       }
//     }
//   `)

//   return (
//     <Layout>
//       <Head title="Blog" />
//       <h1>Blog</h1>
//       <ol className={posts}>
//         {data.allMarkdownRemark.edges.map(edge => {
//           return (
//             <li className={post}>
//               <Link to={`/blog/${edge.node.fields.slug}`}>
//                 <h2>{edge.node.frontmatter.title}</h2>
//                 <p>{edge.node.frontmatter.publishedDate}</p>
//               </Link>
//             </li>
//           )
//         })}
//       </ol>
//     </Layout>
//   )
// }

// export default BlogPage
