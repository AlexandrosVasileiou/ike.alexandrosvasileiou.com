import React from "react"

import Header from "./header"
import Footer from "./footer"
import "../styles/index.scss"

const Layout = props => {
  return (
    <div className="flex flex-col mx-auto my-0">
      <div className="flex-grow">
        <Header />
        {props.children}
      </div>
      <div>
        <Footer />
      </div>
    </div>
  )
}

export default Layout
