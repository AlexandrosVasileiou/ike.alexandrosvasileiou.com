import React from "react"

const Footer = () => {
  const copyrightYear = new Date().getFullYear()

  return (
    <footer className="footer">
      <span className="copyright text-[14px] md:text-[20px]">
        Αλέξανδρος Βασιλείου Μονοπρόσωπη Ι.Κ.Ε. - {copyrightYear}
      </span>
    </footer>
  )
}

export default Footer
