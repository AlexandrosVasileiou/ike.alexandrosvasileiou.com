module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: "class", // or 'media' or 'class',
  mode: "jit",
  important: true,
  theme: {
    extend: {
      // colors: {
      //   background: {
      //     light: "yellow",
      //     dark: "red",
      //     DEFAULT: "blue",
      //   },
      // },
    },
    // screens: {
    //   sm: "500px",
    //   md: [
    //     // Sidebar appears at 768px, so revert to `sm:` styles between 768px
    //     // and 868px, after which the main content area is wide enough again to
    //     // apply the `md:` styles.
    //     { min: "668px", max: "767px" },
    //     { min: "868px" },
    //   ],
    //   lg: "1100px",
    //   xl: "1400px",
    //   // test: { raw: "(max-width: 1024px)" },
    // },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
